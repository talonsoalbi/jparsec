package jparsec.graph;

import java.awt.Color;

import com.orsoncharts.Chart3D;

import jparsec.astronomy.Difraction;
import jparsec.astronomy.TelescopeElement;

public class CreateOrson3DChartTest {

    /**
     * Main test program. Java 8 required to launch.
     * @param args Not used.
     */
	public static void main(String args[]) {
		try {
			TelescopeElement telescope = TelescopeElement.NEWTON_20cm;
            int field = 3;
            double data[][] = Difraction.pattern(telescope, field);
            GridChartElement gridChart = new GridChartElement("Difraction pattern",
                    "offsetX", "offsetY", "RelativeIntensity", GridChartElement.COLOR_MODEL.RED_TO_BLUE,
                    new double[] { -field, field, -field, field }, data,
                    new double[] { 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 }, 400);
            //gridChart.resample(data.length*5, data[0].length*5);
            //ChartSeriesElement3D series = new ChartSeriesElement3D(gridChart);
            CreateOrson3DChart.showChart(CreateOrson3DChart.createSurfacePlot(gridChart, null), 800, 600, "Surface", false);
            

            double x[] = new double[] {0, 0, 3, 5};
            double y[] = new double[] {1, 1, 1, 2};
            double z[] = new double[] {3, 2, 3, 1};
            ChartSeriesElement3D xyz = new ChartSeriesElement3D(x, y, z, "Legend");
            ChartElement3D chart = new ChartElement3D(new ChartSeriesElement3D[] { xyz },
                    "Title\nSubtitle", "X", "Y", "Z");
            chart.imageWidth = 800;
            chart.imageHeight = 600;
            
            Chart3D chart3d = CreateOrson3DChart.createScatterPlot(chart);
            CreateOrson3DChart.addMarkers(chart3d, "2.5", "5.5", "0", "1.5", "2.5", "4", 
            		new Color[] {Color.RED, Color.GREEN, Color.BLUE, Color.CYAN});
            CreateOrson3DChart.showChart(chart3d, 800, 600, "Hola", false);
            CreateOrson3DChart.showChart(CreateOrson3DChart.createScatterWorld(chart), 800, 600, "World 3d", false);
            
            //CreateOrson3DChart.getPicture(chart3d, chart.imageWidth, chart.imageHeight).show("Chart image");
            //CreateOrson3DChart.writeChartAsSVG("/home/alonso/test.svg", chart3d, chart.imageWidth, chart.imageHeight);
            
            String x1[] = new String[] {"a", "b", "c", "d", "e"}, x2[] = x1;
            String y1[] = new String[] {"1", "2", "3", "4", "5"};
            String y2[] = new String[] {"5", "4", "3", "4", "5"};
            ChartSeriesElement s1 = new ChartSeriesElement(x1, y1, "Legend1", true);
            ChartSeriesElement s2 = new ChartSeriesElement(x2, y2, "Legend2", true);
            ChartElement chartElem = new ChartElement(new ChartSeriesElement[] {s1, s2}, 
            		ChartElement.TYPE.CATEGORY_CHART, ChartElement.SUBTYPE.CATEGORY_LINE, 
            		"Title\nSubtitle", "X", "Y", false, 800, 600);
            CreateOrson3DChart.showChart(CreateOrson3DChart.createCategoryPlot(chartElem), 800, 600, "Bar", false);
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
}
