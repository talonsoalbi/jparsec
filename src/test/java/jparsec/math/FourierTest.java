package jparsec.math;


public class FourierTest {
    /**
     * Example of Fourier analysis.
     *
     * @param args Not used.
     */
    public static void main(String[] args) {
    	try {
    		// Example of Fourier analysis: decompose some sample data into a sum of sine/cosine terms with 
    		// optional reduction of less contributing terms
    		
    		// Create more or less random sample data combining sine/cosine in 2^8 = 256 points with 
    		// a high vertical offset
	    	int n = FastMath.multiplyBy2ToTheX(1, 8);
	    	double originalData[] = new double[n];
	    	for (int i=0;i<n;i++) {
	    		double f = i * Constant.TWO_PI / n;
	    		originalData[i] = 1000 + 10 * Math.sin(f);
	    		originalData[i] += 100 * Math.sin(f * 2);
	    		originalData[i] += 20 * Math.sin(f * 5 - Constant.PI_OVER_SIX);
	    		originalData[i] -= 100 * Math.cos(f * 10 + Constant.PI_OVER_FOUR);
	    	}
	    	
	    	// Call to Fourier analysis and report output
	    	Fourier out = Fourier.fourierAnalysis(originalData, 0.05, true);
	    	System.out.println("Found "+out.f.length+" frequencies");
	    	System.out.println("****");
	    	
	    	// Reproduce the approximate input data with only those frequencies
	    	double newData[] = out.reproduceData();
	    	
	    	// Print results: index of array, original data, filtered data, difference of two
	    	for (int index =0;index<out.n;index++) {
		    	System.out.println(index+": "+originalData[index] + " / " + newData[index]+" / "+(newData[index]-originalData[index]));	    		
	    	}

    	} catch (Exception exc) {
    		exc.printStackTrace();
    	}
    }
}
